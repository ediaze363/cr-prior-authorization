﻿using Healthcare.HIS.Domain.Models;
using System.Collections.Generic;

namespace Healthcare.HIS.DataCommon.Persistence
{
    public interface IUserRepository
    {
        User GetById(string userCode);
        IEnumerable<User> GetAll();
    }
}
