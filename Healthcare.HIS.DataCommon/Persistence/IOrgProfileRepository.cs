﻿using Healthcare.HIS.Domain.Enums;
using Healthcare.HIS.Domain.Models;
using System.Collections.Generic;

namespace Healthcare.HIS.DataCommon.Persistence
{
    public interface IOrgProfileRepository
    {
        IEnumerable<OrgProfile> GetById(string profileId);
        IEnumerable<OrgProfile> GetByType(MasterItemType typeId, string companyId = null);
    }
}
