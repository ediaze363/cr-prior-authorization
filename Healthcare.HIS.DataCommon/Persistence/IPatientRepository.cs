﻿using Healthcare.HIS.Domain.Dtos;
using Healthcare.HIS.Domain.Models;
using System.Collections.Generic;

namespace Healthcare.HIS.DataCommon.Persistence
{
    public interface IPatientRepository
    {
        Patient GetById(string documentType, string documentId);
        IList<Approval> GetApprovals(string documentType, string documentId);
        IList<ApprovalDetail> GetCups(string documentType, string documentId);
        string Create(Approval approval);
        IList<ApprovalReportDto> GetApprovalReport();
    }
}
