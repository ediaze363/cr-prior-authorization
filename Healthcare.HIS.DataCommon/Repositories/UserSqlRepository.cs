﻿using Healthcare.HIS.DataCommon.Persistence;

namespace Healthcare.HIS.Business.Repositories
{
    using Domain.Models;
    using Healthcare.HIS.DataCommon.Interfaces;
    using Healthcare.HIS.DataCommon.Repositories;
    using System.Linq;

    public class UserSqlRepository : DataRepository<User, string>, IUserRepository, IDataRepository<User, string>
    {
        private readonly IDatabaseContext _context;

        public UserSqlRepository(IDatabaseContext context) : base(context)
        {
            _context = context;
        }

        public User GetByCode(string userCode)
        {
            User existing = null;

            try
            {
                existing = _context.Users.SingleOrDefault(x => x.Id == userCode);
            }
            catch(System.Exception err)
            {
                System.Console.WriteLine(err);
            }
            return existing;
        }
    }
}
