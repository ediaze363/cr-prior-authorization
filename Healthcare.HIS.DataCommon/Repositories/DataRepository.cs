﻿using Healthcare.HIS.DataCommon.Context;
using Healthcare.HIS.DataCommon.Interfaces;
using Healthcare.HIS.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Healthcare.HIS.DataCommon.Repositories
{
    public abstract class DataRepository<TEntity, TId> : IDataRepository<TEntity, TId>
        where TEntity : class, IEntity<TId>
    {
        private readonly IDatabaseContext _context;
        private readonly DbSet<TEntity> _table;

        public DataRepository(IDatabaseContext context)
        {
            _context = context;
            _table = _context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            _table.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            _table.Remove(entity);
        }

        public void Delete(TId id)
        {
            var existing = _table.FirstOrDefault(e => e.Id.Equals(id));
            if (existing != null)
            {
                _table.Remove(existing);
            }
        }

        public void Update(TEntity entity)
        {
            _table.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public TEntity GetById(TId id)
        {
            return _table.FirstOrDefault(e => e.Id.Equals(id));
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _table.ToList();
        }

        public IEnumerable<TEntity> FindBy(Func<TEntity, bool> predicate)
        {
            return _table.Where(predicate);
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
