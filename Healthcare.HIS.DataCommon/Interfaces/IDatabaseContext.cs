﻿using Healthcare.HIS.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Healthcare.HIS.DataCommon.Interfaces
{
    public interface IDatabaseContext: IRepositoryBase
    {
        DbSet<User> Users { get; set; }
    }
}
