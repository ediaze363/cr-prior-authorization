﻿using Healthcare.HIS.Domain.Models;
using System;
using System.Collections.Generic;

namespace Healthcare.HIS.DataCommon.Interfaces
{
    public interface IDataRepository<TEntity, TId> where TEntity : IEntity<TId>
    {
        void Add(TEntity entity);
        void Delete(TEntity entity);
        void Delete(TId id);
        void Update(TEntity entity);

        //read side (could be in separate Readonly Generic Repository)
        TEntity GetById(TId id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> FindBy(Func<TEntity, bool> predicate);

        //separate method SaveChanges can be helpful when using this pattern with UnitOfWork
        int SaveChanges();
    }
}
