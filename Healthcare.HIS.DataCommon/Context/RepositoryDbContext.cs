﻿using Healthcare.HIS.DataCommon.Interfaces;
using Healthcare.HIS.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Healthcare.HIS.DataCommon.Context
{
    public class RepositoryDbContext : DbContext, IDatabaseContext
    {
        public virtual DbSet<User> Users { get; set; }

        protected RepositoryDbContext(DbContextOptions<RepositoryDbContext> options)
            : base(options) { }

        // Required when are using inherit DbContext
        protected RepositoryDbContext(DbContextOptions options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
