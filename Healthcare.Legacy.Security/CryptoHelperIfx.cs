﻿using System;

namespace Healthcare.Legacy.Security
{
    /// <summary>
    /// Crypto service for DBMS system like informix that allow nonprintable characters for storing a password
    /// </summary>
    public class CryptoHelperIfx : ICryptoHelper
    {
        private const string _invalidParameter = "Invalid parameter value";
        private const string _invalidCharacter = "Invalid character value. Use only printable values";

        public string Encrypt(string inputText, string salt)
        {
            return ComputeHash(inputText, salt, true);
        }

        public string Decrypt(string inputText, string salt)
        {
            return ComputeHash(inputText, salt, false);
        }

        private string ComputeHash(string inputText, string salt, bool toEncrypt = true)
        {
            bool flag = false;
            if (salt == null || salt == "")
            {
                throw new ArgumentException(_invalidParameter, "salt");
            }

            if (toEncrypt)
            {
                for (int startIndex = 0; startIndex < inputText.Length; ++startIndex)
                {
                    switch (inputText.Substring(startIndex, 1))
                    {
                        case "[":
                        case "|":
                        case "}":
                        case "~":
                        case "¶":
                        case "¬":
                        case "°":
                        case "©":
                        case "§":
                        case "]":
                        case " ":
                            flag = true;
                            break;
                    }
                    switch (Convert.ToInt32(Convert.ToChar(inputText.Substring(startIndex, 1))))
                    {
                        case 13:
                            flag = true;
                            break;
                        case 247:
                            flag = true;
                            break;
                    }
                }
            }
            if (flag)
            {
                throw new ArgumentException(_invalidCharacter);
            }
            string[] strArray = new string[95];
            int index1 = 1;
            string str1 = "8x3p5BeabcdfghijklmnoqrstuvwyzACDEFGHIJKLMNOPQRSTUVWXYZ 1246790-.#/\\!@$<>&*()[]{}';:,?=+`^|%_";
            int length1 = str1.Length;
            strArray[1] = str1;
            for (int index2 = 2; index2 <= length1; ++index2)
            {
                string str2 = strArray[index1].Substring(0, 1);
                string str3 = strArray[index1].Substring(1);
                strArray[index2] = str3 + str2;
                ++index1;
            }
            string str4 = inputText;
            int length2 = inputText.Length;
            int length3 = salt.Length;
            string str5 = "";
            int startIndex1 = 0;
            for (int startIndex2 = 0; startIndex2 < length2; ++startIndex2)
            {
                string str2 = str4.Substring(startIndex2, 1);
                if (toEncrypt)
                {
                    switch (Convert.ToInt32(Convert.ToChar(str2)))
                    {
                        case 13:
                            str2 = "§";
                            break;
                        case 32:
                            str2 = "©";
                            break;
                        case 39:
                            str2 = "¶";
                            break;
                        case 124:
                            str2 = "¬";
                            break;
                        case 126:
                            str2 = "°";
                            break;
                    }
                }
                else
                {
                    switch (Convert.ToInt32(Convert.ToChar(str2)))
                    {
                        case 13:
                            str2 = "§";
                            break;
                        case 32:
                            str2 = "©";
                            break;
                        case 39:
                            str2 = "¶";
                            break;
                    }
                    switch (str2)
                    {
                        case "¶":
                            str2 = "'";
                            break;
                    }
                }
                int startIndex3 = str1.IndexOf(str2);
                string str3 = salt.Substring(startIndex1, 1);
                for (int index2 = 1; index2 <= length1; ++index2)
                {
                    if (startIndex3 != -1)
                    {
                        if (strArray[index2].Substring(startIndex3, 1) == str3)
                        {
                            string str6 = strArray[index2].Substring(0, 1);
                            if (Convert.ToInt32(Convert.ToChar(str6)) == 39)
                                str6 = "¶";
                            str5 += str6;
                            break;
                        }
                    }
                    else
                    {
                        if (toEncrypt)
                        {
                            string str6 = str2;
                            str5 += str6;
                            break;
                        }
                        string str7 = str2;
                        switch (str7)
                        {
                            case "°":
                                str7 = Convert.ToChar(126).ToString();
                                break;
                            case "¬":
                                str7 = Convert.ToChar(124).ToString();
                                break;
                            case "©":
                                str7 = Convert.ToChar(32).ToString();
                                break;
                            case "¶":
                                str7 = Convert.ToChar(39).ToString();
                                break;
                            case "§":
                                str7 = Convert.ToChar(13).ToString();
                                break;
                        }
                        str5 += str7;
                        break;
                    }
                }
                ++startIndex1;
                if (startIndex1 == length3)
                    startIndex1 = 0;
            }
            return str5;
        }
    }
}
