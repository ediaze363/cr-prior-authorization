﻿namespace Healthcare.Legacy.Security
{
    /// <summary>
    /// Define common methods for Cryptography service
    /// </summary>
    public interface ICryptoHelper
    {
        string Encrypt(string inputText, string salt);
        string Decrypt(string inputText, string salt);
    }
}
