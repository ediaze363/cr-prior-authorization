﻿using System;

namespace Healthcare.Legacy.Security
{
    /// <summary>
    /// Crypto service for DBMS system like SqlServer and Oracle that allow nonprintable characters for storing a password
    /// </summary>
    public class CryptoHelperOraMsSql : ICryptoHelper
    {
        private const string _invalidParameter = "Invalid parameter value";

        public string Encrypt(string inputText, string salt)
        {
            return ComputeHash(inputText, salt, true);
        }

        public string Decrypt(string inputText, string salt)
        {
            return ComputeHash(inputText, salt, false);
        }

        private string ComputeHash(string inputText, string salt, bool toEncrypt = true)
        {
            if (salt == null || salt == "")
            {
                throw new ArgumentException(_invalidParameter, "salt");
            }
            if (toEncrypt)
            {
                int num1 = 1;
                int length2 = inputText.Length;
                string str1 = "";
                for (int index = 1; index <= length2; ++index)
                {
                    int num2 = Convert.ToInt32(Convert.ToChar(inputText.Substring(index - 1, 1))) + Convert.ToInt32(Convert.ToChar(salt.Substring(num1 - 1, 1)));
                    while (num2 > 123)
                    {
                        if (num2 > 123)
                            num2 -= 123;
                    }
                    switch (num2)
                    {
                        case 13:
                            num2 = 124;
                            break;
                        case 32:
                            num2 = 125;
                            break;
                        case 39:
                            num2 = 126;
                            break;
                        case 96:
                            num2 = (int)sbyte.MaxValue;
                            break;
                    }
                    string str2 = Convert.ToChar(num2).ToString();
                    str1 += str2;
                    ++num1;
                    if (num1 > salt.Length)
                        num1 = 1;
                }
                return str1;
            }
            int num3 = 1;
            int length4 = inputText.Length;
            string str3 = "";
            for (int index = 1; index <= length4; ++index)
            {
                int num1 = Convert.ToInt32(Convert.ToChar(inputText.Substring(index - 1, 1)));
                switch (num1)
                {
                    case 124:
                        num1 = 13;
                        break;
                    case 125:
                        num1 = 32;
                        break;
                    case 126:
                        num1 = 39;
                        break;
                    case (int)sbyte.MaxValue:
                        num1 = 96;
                        break;
                }
                int int32 = Convert.ToInt32(Convert.ToChar(salt.Substring(num3 - 1, 1)));
                int num2 = num1 - int32;
                while (num2 < 0)
                {
                    if (num2 < 0)
                        num2 += 123;
                }
                string str1 = Convert.ToChar(num2).ToString();
                str3 += str1;
                ++num3;
                if (num3 > salt.Length)
                    num3 = 1;
            }
            return str3;
        }        
    }
}
