﻿using System;

namespace Healthcare.Legacy.Security
{
    public class CryptoHelper : ICryptoHelper
    {
        public string Decrypt(string inputText, string salt)
        {
            return ComputeHash(inputText, salt);
        }

        public string Encrypt(string inputText, string salt)
        {
            return ComputeHash(inputText, salt);
        }

        private string ComputeHash(string stringValue, string key)
        {
            string str1 = string.Empty;
            string empty2 = string.Empty;
            int length1 = key.Length;
            for (int startIndex = 0; startIndex < length1; ++startIndex)
            {
                string str2 = key.Substring(startIndex, 1);
                empty2 += (string)(object)Convert.ToInt16(Convert.ToChar(str2));
            }
            int length2 = stringValue.Length;
            for (int index = 1; index <= length2; ++index)
            {
                int num1 = 254 - (int)Convert.ToInt16(Convert.ToChar(stringValue.Substring(index - 1, 1)));
                string str2 = index.ToString();
                int int32 = Convert.ToInt32(str2.Substring(str2.Length - 1, 1));
                int int16 = (int)Convert.ToInt16(empty2.Substring(int32 > 0 ? int32 - 1 : int32, 1));
                int num2 = num1 + int16 + int32;
                str1 = string.Format("{0}{1}", (object)str1, (object)Convert.ToChar(num2));
            }
            return str1;
        }
    }
}
