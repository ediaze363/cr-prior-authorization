﻿using Healthcare.HIS.Business.Interfaces;
using Healthcare.HIS.DataCommon.Persistence;
using Healthcare.HIS.Domain.Models;
using Healthcare.Legacy.Security;

namespace Healthcare.HIS.Business.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;
        private readonly ICryptoHelper _cryptoHelper;

        public UserService(IUserRepository repository, ICryptoHelper cryptoHelper)
        {
            _repository = repository;
            _cryptoHelper = cryptoHelper;
        }

        public User Authenticate(string username, string password, string passwordSalt)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return null;
            }

            var user = GetById(username);

            if (user == null)
            {
                return null;
            }
            if (!VerifyPassword(password, user.PasswordHash, passwordSalt))
            {
                return null;
            }
            return user;
        }

        public User GetById(string id)
        {
            return _repository.GetById(id);
        }
        
        private bool VerifyPassword(string password, string storedHash, string salt)
        {
            var passwordHash = _cryptoHelper.Encrypt(password, salt);
            return string.Equals(storedHash, passwordHash);
        }
    }
}
