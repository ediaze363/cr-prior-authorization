﻿using Healthcare.HIS.Business.Interfaces;
using Healthcare.HIS.DataCommon.Persistence;
using Healthcare.HIS.Domain.Enums;
using Healthcare.HIS.Domain.Models;
using System.Collections.Generic;

namespace Healthcare.HIS.Business.Services
{
    public class OrgProfileService : IOrgProfileService
    {
        private readonly IOrgProfileRepository _repository;

        public OrgProfileService(IOrgProfileRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<OrgProfile> GetById(string categoryId)
        {
            return _repository.GetById(categoryId);
        }

        public IEnumerable<OrgProfile> GetByType(MasterItemType typeId, string companyId = null)
        {
            return _repository.GetByType(typeId, companyId);
        }
    }
}
