﻿using Healthcare.HIS.Business.Interfaces;
using Healthcare.HIS.DataCommon.Persistence;
using Healthcare.HIS.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Healthcare.HIS.Business.Services
{
    public class PatientService: IPatientService
    {
        private readonly IPatientRepository _repository;

        public PatientService(IPatientRepository repository)
        {
            _repository = repository;
        }

        public Patient GetById(string documentType, string documentId)
        {
            try
            {
                var patientInfo = _repository.GetById(documentType, documentId);
                if(patientInfo != null)
                {
                    patientInfo.HasAllowedAge = patientInfo.Age >= 18;
                }                
                return patientInfo;
            }
            catch(Exception err)
            {
                if (err.Message.Contains("404."))
                {
                    return null;
                }
                throw;
            }
        }

        public IList<Approval> GetApprovals(string documentType, string documentId)
        {
            var approvals = _repository.GetApprovals(documentType, documentId);
            var approvalDetail = _repository.GetCups(documentType, documentId);

            if (approvals != null && approvalDetail != null)
            {
                foreach (var approval in approvals)
                {
                    approval.Details = approvalDetail
                        .Where(d => d.ParentId == approval.Id)
                        .OrderBy(d => d.Line).ToList();
                }
            }            

            return approvals;
        }

        public Approval Create(Approval approval)
        {
            approval.Id = Guid.NewGuid().ToString();
            approval.Active = "1";
            approval.ApprovalId = _repository.Create(approval);
            return approval;
        }

        public IList<Domain.Dtos.ApprovalReportDto> GetApprovalReport()
        {
            return _repository.GetApprovalReport();
        }
    }
}
