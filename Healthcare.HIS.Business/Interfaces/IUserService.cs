﻿using Healthcare.HIS.Domain.Models;

namespace Healthcare.HIS.Business.Interfaces
{
    public interface IUserService
    {
        User Authenticate(string username, string password, string passwordSalt);
        User GetById(string id);
    }
}
