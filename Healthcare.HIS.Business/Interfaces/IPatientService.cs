﻿using Healthcare.HIS.Domain.Models;
using System.Collections.Generic;

namespace Healthcare.HIS.Business.Interfaces
{
    public interface IPatientService
    {
        Patient GetById(string documentType, string documentId);
        IList<Approval> GetApprovals(string documentType, string documentId);
        Approval Create(Approval approval);
        IList<Domain.Dtos.ApprovalReportDto> GetApprovalReport();
    }
}
