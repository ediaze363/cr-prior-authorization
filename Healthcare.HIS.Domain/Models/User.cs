namespace Healthcare.HIS.Domain.Models
{
    public class User: IEntity<string>
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }        
        public string PasswordHash { get; set; }
    }
}