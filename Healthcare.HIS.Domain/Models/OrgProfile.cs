﻿namespace Healthcare.HIS.Domain.Models
{
    public class OrgProfile
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
