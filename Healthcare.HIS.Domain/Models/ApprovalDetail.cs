﻿namespace Healthcare.HIS.Domain.Models
{
    public class ApprovalDetail
    {
        public string Id { get; set; }
        public int Line { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
        public string StatusId { get; set; }
        public string StatusName { get; set; }
        public string ParentId { get; set; }
    }
}
