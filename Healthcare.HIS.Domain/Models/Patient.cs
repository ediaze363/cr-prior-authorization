﻿using System;

namespace Healthcare.HIS.Domain.Models
{
    public class Patient
    {
        public string DocumentType { get; set; }
        public string DocumentId { get; set; }
        public long History { get; set; }
        public long? Visit { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public string CityCode { get; set; }
        public string CityName { get; set; }
        public DateTime CurrentDate { get; set; }
        public string AdmissionType { get; set; }
        public DateTime Birthdate { get; set; }
        public bool? HasAllowedAge { get; set; }
    }
}
