﻿using System;

namespace Healthcare.HIS.Domain.Models
{
    public interface IEntity<TId>
    {
        TId Id { get; set; }
    }
}
