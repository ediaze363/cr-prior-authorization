﻿namespace Healthcare.HIS.Domain.Enums
{
    public enum MasterItemType
    {
        InDia,
        InEsp,
        InEmp,
        InMed,
        AutTip,
        AutCup,
        AutIps,
        AutEmp
    }
}
