﻿namespace Healthcare.HIS.Domain.Dtos
{
    public class PatientDto: Models.Patient
    {
        public long Id { get; set; }
        public string NationalId
        {
            get { return $"{DocumentType}-{DocumentId}"; }
        }
        public string Today
        {
            get { return CurrentDate.ToString("yyyy/MM/dd"); }
        }
        public string Now
        {
            get { return CurrentDate.ToString("HH:mm"); }
        }
        public string DateOfBirth
        {
            get { return Birthdate.ToString("yyyy/MM/dd"); }
        }
    }
}
