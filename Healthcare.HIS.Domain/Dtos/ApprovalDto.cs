﻿using Healthcare.HIS.Domain.Models;
using System.Collections.Generic;

namespace Healthcare.HIS.Domain.Dtos
{
    public class ApprovalDto
    {
        public string Id { get; set; }
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string ApprovalId { get; set; }
        public string DocumentType { get; set; }
        public string DocumentId { get; set; }
        public string Birthdate { get; set; }
        public int Age { get; set; }
        public long History { get; set; }
        public int Visit { get; set; }
        public string StateId { get; set; }
        public string CityId { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalName { get; set; }
        public string PhysicianId { get; set; }
        public string SpecialtyId { get; set; }
        public string DiagnosisId { get; set; }
        public string Created { get; set; }
        public string StatusId { get; set; }
        public string StatusName { get; set; }
        public string UserId { get; set; }
        public string Active { get; set; }
        public string HeadquarterId { get; set; }
        public string TaxpayerId { get; set; }

        public IList<ApprovalDetailDto> Details { get; set; }

        public string HistoryVisit
        {
            get { return $"{History}-{Visit}"; }
        }
    }
}
