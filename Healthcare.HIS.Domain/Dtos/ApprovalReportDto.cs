﻿namespace Healthcare.HIS.Domain.Dtos
{
    public class ApprovalReportDto
    {
        public string C_EMPRESA { get; set; }
        public string N_EMPRESA { get; set; }
        public string NIT_EMPRESA { get; set; }
        public string N_AUTORIZACION { get; set; }
        public string TIPO_IDE { get; set; }
        public string IDE { get; set; }
        public string N_PACIENTE { get; set; }
        public string HISTORIA { get; set; }
        public string INGRESO { get; set; }
        public string TIPO_AUT { get; set; }
        public string N_TIPO { get; set; }
        public string C_MEDICO { get; set; }
        public string N_MEDICO { get; set; }
        public string C_ESPECIALIDAD { get; set; }
        public string N_ESPECIALIDAD { get; set; }
        public string C_DIAGNOSTICO { get; set; }
        public string N_DIAGNOSTICO { get; set; }
        public string ITEM { get; set; }
        public string C_CUP { get; set; }
        public string N_CUP { get; set; }
        public string TP_CUP { get; set; }
        public string GRU_CUP { get; set; }
    }
}
