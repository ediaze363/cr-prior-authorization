CREATE TABLE "BASDAT"."INDIA" 
(
  "DIACOD"   VARCHAR2(7) NOT NULL ENABLE,
  "DIANOM"   VARCHAR2(60),
  "DIADES"   VARCHAR2(255),
  "DIACIE"   VARCHAR2(3),
  "DIASUB"   VARCHAR2(3),
  "DIAGDX"   VARCHAR2(2),
  "DIASEC"   VARCHAR2(3),
  "DIANIV"   VARCHAR2(3),
  "DIAEDI"   NUMBER(5,2),
  "DIAEDS"   NUMBER(5,2),
  "DIASEX"   VARCHAR2(1),
  "DIATRS"   VARCHAR2(1),
  "DIANOO"   VARCHAR2(1),
  "DIAACT"   VARCHAR2(1),
  "DIAUAD"   VARCHAR2(10),
  "DIAFAD"   DATE,
  "DIAUMO"   VARCHAR2(10),
  "DIAFMO"   DATE,
  "DIAPYP"   VARCHAR2(1),
  "DIAENF"   VARCHAR2(4),
  "DIACON"   VARCHAR2(1),
  "DIACAE"   VARCHAR2(1) NOT NULL ENABLE,
  "DIADUP"   VARCHAR2(1) NOT NULL ENABLE,
  CONSTRAINT "INDIA_P1" PRIMARY KEY ("DIACOD") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE (INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "TSI_BASDAT" ENABLE
)
SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE (INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "TSD_BASDAT"

