CREATE TABLE "BASDAT"."SITEA" 
(
  "TEACOD"   VARCHAR2(3) NOT NULL ENABLE,
  "TEANOM"   VARCHAR2(20) NOT NULL ENABLE,
  "TEANIV"   NUMBER(6,0),
  "TEACIA"   VARCHAR2(1) NOT NULL ENABLE,
  "TEAUAD"   VARCHAR2(10) NOT NULL ENABLE,
  "TEAFAD"   DATE NOT NULL ENABLE,
  CONSTRAINT "SITEA_P1" PRIMARY KEY ("TEACOD") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE (INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "TSI_BASDAT" ENABLE
)
SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE (INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "TSD_BASDAT"

