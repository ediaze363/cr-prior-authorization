CREATE TABLE "SICTEDET" 
(
  "CTEDETCOD"   VARCHAR2(6) NOT NULL ENABLE,
  "CTEDETPAR"   VARCHAR2(20) NOT NULL ENABLE,
  "CTEDETDES"   VARCHAR2(100),
  "CTEDETACT"   VARCHAR2(1),
  CONSTRAINT "SICTEDET_P1" PRIMARY KEY ("CTEDETCOD", "CTEDETPAR") USING INDEX
)