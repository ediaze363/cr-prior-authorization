CREATE TABLE "BASDAT"."SICIA" 
(
  "CIACOD"   VARCHAR2(5) NOT NULL ENABLE,
  "CIAMUL"   VARCHAR2(1),
  "CIANOM"   VARCHAR2(100),
  "CIANIT"   VARCHAR2(20),
  "CIATIP"   VARCHAR2(1),
  "CIADIR"   VARCHAR2(100),
  "CIACOC"   VARCHAR2(2),
  "CIANOC"   VARCHAR2(30),
  "CIATEL"   VARCHAR2(25),
  "CIAIND"   VARCHAR2(6),
  "CIALON"   NUMBER(10,0),
  "CIANIV"   NUMBER(10,0),
  "CIACON"   VARCHAR2(1),
  "CIABMP"   VARCHAR2(60),
  "CIAGCH"   VARCHAR2(1),
  "CIAGRA"   VARCHAR2(1),
  "CIAFAX"   VARCHAR2(20),
  "CIAMAI"   VARCHAR2(60),
  "CIARAZ"   VARCHAR2(100),
  "CIAREG"   VARCHAR2(1),
  "CIARET"   VARCHAR2(1),
  "CIACEN"   VARCHAR2(1),
  "CIAPAI"   VARCHAR2(15),
  "CIAWEB"   VARCHAR2(60),
  "CIARED"   VARCHAR2(1),
  "CIANAC"   VARCHAR2(15),
  "CIAHAB"   VARCHAR2(12),
  "CIAMUN"   VARCHAR2(5),
  "CIALEG"   VARCHAR2(70),
  "CIAALL"   NUMBER(4,0),
  "CIAANL"   NUMBER(4,0),
  "CIATEA"   VARCHAR2(3),
  "CIAEAP"   VARCHAR2(5),
  "CIAEJU"   VARCHAR2(5),
  "CIAIUN"   VARCHAR2(1),
  "CIACIA"   VARCHAR2(1),
  "CIAACT"   VARCHAR2(1),
  "CIAPPA"   VARCHAR2(1),
  "CIAABR"   VARCHAR2(20),
  "CIAIPS"   VARCHAR2(12),
  "CIARUC"   VARCHAR2(20),
  "CIAUAD"   VARCHAR2(10),
  "CIAFAD"   DATE,
  "CIAUMO"   VARCHAR2(10),
  "CIAFMO"   DATE,
  "CIAPCU"   VARCHAR2(5),
  "CIAIDE"   VARCHAR2(1),
  "CIAMUF"   VARCHAR2(1) NOT NULL ENABLE,
  "CIAUFU"   VARCHAR2(5),
  "CIATRE"   VARCHAR2(3),
  "CIAIRE"   VARCHAR2(20),
  "CIALRE"   VARCHAR2(5),
  "CIADRE"   VARCHAR2(5),
  "CIANAU"   VARCHAR2(20),
  "CIAFAI"   DATE,
  "CIAFAF"   DATE,
  "CIAAPA"   VARCHAR2(50),
  "CIAIMA"   NUMBER(10,0),
  "CIATKN"   VARCHAR2(250),
  "CIACMA"   VARCHAR2(35),
  "CIACZO"   VARCHAR2(40),
  "CIACDI"   VARCHAR2(200),
  "CIARMA"   VARCHAR2(35),
  "CIARTE"   VARCHAR2(4),
  "CIARRE"   VARCHAR2(7),
  "CIABAR"   VARCHAR2(10),
  CONSTRAINT "SICIA_I1" PRIMARY KEY ("CIACOD") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE (INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "TSI_BASDAT" ENABLE,
  CONSTRAINT "SICIA_F1" FOREIGN KEY ("CIATEA") REFERENCES "BASDAT"."SITEA" ("TEACOD") ENABLE
)
SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE (INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "TSD_BASDAT";

