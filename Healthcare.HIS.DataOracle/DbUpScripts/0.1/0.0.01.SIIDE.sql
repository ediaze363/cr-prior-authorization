CREATE TABLE "BASDAT"."SIIDE" 
   (	"IDECOD" VARCHAR2(10) NOT NULL ENABLE, 
	"IDENOM" VARCHAR2(20), 
	"IDEAP1" VARCHAR2(15), 
	"IDEAP2" VARCHAR2(15), 
	"IDEOFI" VARCHAR2(4), 
	"IDEVAR" VARCHAR2(1), 
	"IDEACT" VARCHAR2(1), 
	"IDEUAD" VARCHAR2(10), 
	"IDEFAD" DATE, 
	"IDEUMO" VARCHAR2(10), 
	"IDEFMO" DATE, 
	"IDETID" VARCHAR2(3), 
	"IDEIDE" VARCHAR2(20), 
	"IDEREG" VARCHAR2(12), 
	"IDECLA" VARCHAR2(10), 
	"IDEFCL" DATE, 
	"IDECAD" VARCHAR2(1), 
	"IDEFCC" DATE, 
	"IDEESP" VARCHAR2(3), 
	"IDECEX" VARCHAR2(20), 
	"IDEOFC" VARCHAR2(4), 
	"IDEMED" VARCHAR2(5), 
	"IDEBLQ" VARCHAR2(1), 
	"IDEUFB" DATE, 
	"IDEDES" VARCHAR2(100), 
	"IDEUFI" DATE, 
	"IDEIND" VARCHAR2(5), 
	"IDETEL" VARCHAR2(7), 
	"IDEEXT" VARCHAR2(5), 
	"IDECEL" VARCHAR2(10), 
	"IDEITA" VARCHAR2(1) NOT NULL ENABLE, 
	"IDERFI" VARCHAR2(255), 
	"IDEMAI" VARCHAR2(60), 
	"IDEFOL" VARCHAR2(7), 
	"IDELIB" VARCHAR2(7), 
	"IDERAD" NUMBER(10,0), 
	 CONSTRAINT "SIIDE_I1" PRIMARY KEY ("IDECOD")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TSI_BASDAT"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TSD_BASDAT" ;
