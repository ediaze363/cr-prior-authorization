DECLARE 
    expiration DATE := Add_months(SYSDATE,3); 
    
BEGIN  
    INSERT INTO siide 
                (idecod, ideita, idenom, ideap1, ideap2, ideact, idecla, idefcl, idefcc, idecad) 
                VALUES 
                ('cremed', 'S', 'Cl�nica Nuestra',  'Se�ora', 'de los Remedios', 'S', '****', expiration, expiration, 'N'); 
     
    dbms_output.put_line('Mock user added'); 
  END;
  /