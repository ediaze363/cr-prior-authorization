CREATE OR REPLACE PROCEDURE AUT_P_GET_SEQUENCE(
  i_taxpayerId IN AUT_SEC.AUTNIT%TYPE,
  c_result OUT SYS_REFCURSOR)
AS
  l_sequence AUT_ENC.ENCAUTAUT%TYPE;
BEGIN
    SELECT MAX(autsec)
    INTO l_sequence
    FROM aut_sec
    WHERE autnit = i_taxpayerId;

    IF nvl(l_sequence, -1) = -1 THEN
        l_sequence := 1;
        INSERT INTO aut_sec (autnit, autsec) 
        VALUES (i_taxpayerId, l_sequence);
    ELSE
        l_sequence := l_sequence + 1;
        UPDATE aut_sec
        SET autsec = l_sequence
        WHERE autnit = i_taxpayerId;
    END IF;

    OPEN c_result FOR
       SELECT TO_CHAR(l_sequence) Id, i_taxpayerId Name
       FROM DUAL;
END;