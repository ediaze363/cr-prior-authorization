CREATE TABLE "BASDAT"."AUT_ENC" 
(
  "ENCAUTCOD"   VARCHAR2(36) NOT NULL ENABLE,
  "ENCAUTEMP"   VARCHAR2(20),
  "ENCAUTAUT"   NUMBER(10,0),
  "ENCAUTIDE"   VARCHAR2(20),
  "ENCAUTTID"   VARCHAR2(3),
  "ENCAUTFNA"   DATE,
  "ENCAUTHIS"   NUMBER(10,0),
  "ENCAUTNUM"   NUMBER(6,0),
  "ENCAUTMUN"   VARCHAR2(5),
  "ENCAUTTIP"   VARCHAR2(2),
  "ENCAUTMED"   VARCHAR2(5),
  "ENCAUTESP"   VARCHAR2(3),
  "ENCAUTDIA"   VARCHAR2(7),
  "ENCAUTUAD"   VARCHAR2(10),
  "ENCAUTFAD"   DATE,
  "ENCAUTUMO"   VARCHAR2(10),
  "ENCAUTFMO"   DATE,
  "ENCAUTEST"   VARCHAR2(1),
  "ENCAUTIPS"   VARCHAR2(2),
  CONSTRAINT "AUT_ENC_PK" PRIMARY KEY ("ENCAUTCOD") USING INDEX (CREATE UNIQUE INDEX "BASDAT"."ENC_AUT_I1" ON "BASDAT"."AUT_ENC" ("ENCAUTCOD") PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE (INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "TSI_BASDAT") ENABLE
)
SEGMENT CREATION IMMEDIATE PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE (INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "TSI_BASDAT";

ALTER TABLE BASDAT.AUT_ENC ADD ENCAUTIPS VARCHAR2(2);

ALTER TABLE BASDAT.AUT_ENC DROP COLUMN COLUMN2;
