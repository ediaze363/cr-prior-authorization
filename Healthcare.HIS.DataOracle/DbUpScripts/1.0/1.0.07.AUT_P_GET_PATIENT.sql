CREATE OR REPLACE PROCEDURE AUT_P_GET_PATIENT(
  i_documentType IN ABPAC.PACTID%TYPE,
  i_documentId IN ABPAC.PACIDE%TYPE,
  c_patient OUT SYS_REFCURSOR)
AS
  l_rowCount        NUMBER;
  l_historyNumber   INPAC.PACHIS%TYPE;
  l_visitNumber     INPAC.PACNUM%TYPE;
  l_admissionType    CHAR(2) := NULL;
BEGIN  
  SELECT COUNT(1) INTO l_rowCount
  FROM abpac pac
  WHERE pac.pactid = i_documentType
  AND   pac.pacide = i_documentId;
    
  IF l_rowCount = 0 THEN
    RAISE_APPLICATION_ERROR (-20404, '404.Paciente no admisionado.');
  END IF;
  
  IF l_historyNumber IS NULL THEN
    -- Pacientes Hospitalizados
    BEGIN
      SELECT hos.pachis, hos.pacnum 
        INTO l_historyNumber, l_visitNumber
      FROM abpac pac
        INNER JOIN inpac hos ON pac.pachis = hos.pachis
      WHERE pac.pactid = i_documentType
      AND   pac.pacide = i_documentId
      AND   ROWNUM <= 1;
      l_admissionType := '03';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_historyNumber := NULL;
    END;
  END IF;
  
  IF l_historyNumber IS NULL THEN
    -- pacientes ambulatorios
    BEGIN
      SELECT amb.movhis, amb.movnum 
        INTO l_historyNumber, l_visitNumber
      FROM abpac pac
        INNER JOIN aymov amb ON pac.pachis = amb.movhis
      WHERE pac.pactid = i_documentType
      AND   pac.pacide = i_documentId
      AND   amb.movfue = '03'
      AND   ROWNUM <= 1
      AND   NOT EXISTS (SELECT 1
                        FROM aymovegr egr
                        WHERE egr.movegrfue = amb.movfue
                        AND   amb.movdoc = egr.movegrdoc
                        AND   movegranu = '0');
      l_admissionType := '02';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_historyNumber := NULL;
    END;
  END IF;
  
  OPEN c_patient FOR
  SELECT
       NVL(pac.pactid,'') DocumentType
       ,NVL(pac.pacide,'') DocumentId
       ,pac.pachis History
       ,l_visitNumber Visit
       ,REPLACE(NVL(pac.pacnom,'') || ' ' || NVL(pac.pacap1,'') || ' ' || NVL(pac.pacap2,''),'  ',' ') Name
       ,TRUNC(MONTHS_BETWEEN(SYSDATE, pac.pacnac) / 12) Age
       ,pac.pacdir Address
       ,pac.pactel PhoneNumber
       ,dep.depcod StateCode
       ,dep.depnom StateName
       ,mun.muncod CityCode
       ,mun.munnom CityName
       ,SYSDATE CurrentDate
       ,l_admissionType AdmissionType
       ,pac.pacnac Birthdate
  FROM abpac pac
    INNER JOIN inmun mun ON mun.muncod = pac.pacmun
    INNER JOIN indep dep ON dep.depcod = mun.mundep
  WHERE pac.pactid = i_documentType
    AND pac.pacide = i_documentId;
 
END;
