﻿using Healthcare.HIS.DataCommon.Context;
using Healthcare.HIS.DataOracle.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Healthcare.HIS.DataOracle.Context
{
    public class OracleDbContext: RepositoryDbContext
    {
        readonly IConfiguration _config;

        public OracleDbContext(DbContextOptions<OracleDbContext> options, IConfiguration config)
            : base(options)
        {
            _config = config;
        }

        public IConfiguration Config => _config;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //string defaultSchema = _config["AppSettings:DefaultSchema"];
            // modelBuilder.HasDefaultSchema(defaultSchema);
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
    }
}
