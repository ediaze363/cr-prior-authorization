﻿using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Data;

namespace Healthcare.HIS.DataOracle.Persistence
{
    public class OracleRepository
    {
        private readonly IConfiguration _configuration;
        private string _connectionString;

        private string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(_connectionString))
                {
                    var connectionName = _configuration.GetSection("AppSettings").GetSection("DefaultConnection").Value;
                    _connectionString = _configuration.GetConnectionString(connectionName);
                }
                return _connectionString;
            }
        }

        public OracleRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IDbConnection GetConnection()
        {
            var database = new OracleConnection(ConnectionString);
            database.Open();
            return database;
        }
    }
}
