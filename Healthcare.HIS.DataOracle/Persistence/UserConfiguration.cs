﻿using Healthcare.HIS.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Healthcare.HIS.DataOracle.Persistence
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> entity)
        {
            entity.ToTable("siide");
            entity.Property(e => e.Id).HasColumnName("idecod").HasColumnType("varchar").HasMaxLength(10);
            entity.Property(e => e.FirstName).HasColumnName("idenom").HasColumnType("varchar").HasMaxLength(20);
            entity.Property(e => e.PasswordHash).HasColumnName("idecla").HasColumnType("varchar").HasMaxLength(10);
        }
    }
}
