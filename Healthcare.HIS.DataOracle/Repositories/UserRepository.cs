﻿using Dapper;
using Healthcare.HIS.DataCommon.Persistence;
using Healthcare.HIS.DataOracle.Helpers;
using Healthcare.HIS.DataOracle.Persistence;
using Healthcare.HIS.Domain.Models;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Healthcare.HIS.DataOracle.Repositories
{
    public class UserRepository : OracleRepository, IUserRepository
    {
        public UserRepository(IConfiguration configuration): base(configuration)
        {
        }

        public IEnumerable<User> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public User GetById(string userCode)
        {
            User selected = null;
            using (var database = GetConnection())
            {
                var query = Resources.SqlUser.ReadById;
                var args = new OracleDynamicParameters();
                args.Add("empId", OracleDbType.Varchar2, ParameterDirection.Input, userCode);
                selected = SqlMapper.Query<User>(database, query, param: args, commandType: CommandType.Text).FirstOrDefault();
            }
            return selected;
        }
    }
}
