﻿using Dapper;
using Healthcare.HIS.DataCommon.Persistence;
using Healthcare.HIS.DataOracle.Helpers;
using Healthcare.HIS.DataOracle.Persistence;
using Healthcare.HIS.Domain.Enums;
using Healthcare.HIS.Domain.Models;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Data;

namespace Healthcare.HIS.DataOracle.Repositories
{
    public class OrgProfileRepository : OracleRepository, IOrgProfileRepository
    {
        public OrgProfileRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public IEnumerable<OrgProfile> GetById(string categoryId)
        {
            IEnumerable<OrgProfile> selected = null;
            categoryId = categoryId ?? string.Empty;
            using (var database = GetConnection())
            {
                var query = Resources.SqlOrgProfile.GetOrgProfiles;
                var args = new OracleDynamicParameters();
                args.Add("profileId", OracleDbType.Varchar2, ParameterDirection.Input, categoryId, categoryId.Length);
                selected = SqlMapper.Query<OrgProfile>(database, query, param: args, commandType: CommandType.Text);
            }
            return selected;
        }

        public IEnumerable<OrgProfile> GetByType(MasterItemType typeId, string companyId = null)
        {
            IEnumerable<OrgProfile> types = null;
            var queries = new Dictionary<MasterItemType, string>()
            {
                { MasterItemType.InDia, Resources.SqlOrgProfile.GetDiagnoses },
                { MasterItemType.InEsp, Resources.SqlOrgProfile.GetSpecialities },
                { MasterItemType.InEmp, Resources.SqlOrgProfile.GetCompanies },
                { MasterItemType.InMed, Resources.SqlOrgProfile.GetPhisicians },
                { MasterItemType.AutEmp, Resources.SqlOrgProfile.GetCompanyWithApprovals },
                { MasterItemType.AutTip, Resources.SqlOrgProfile.GetApprovalTypes },
                { MasterItemType.AutCup, Resources.SqlOrgProfile.GetCupsByNit },
                { MasterItemType.AutIps, Resources.SqlOrgProfile.GetIpsByNit }
            };
            using (var database = GetConnection())
            {
                var query = queries[typeId];
                OracleDynamicParameters args = null;

                if (!string.IsNullOrEmpty(companyId))
                {
                    args = new OracleDynamicParameters();
                    args.Add("companyNit", OracleDbType.Varchar2, ParameterDirection.Input, companyId, companyId.Length);

                }

                types = SqlMapper.Query<OrgProfile>(database, query, param: args, commandType: CommandType.Text);
            }

            types = (types != null && (types as List<OrgProfile>).Count == 0) ? null : types;
            return types;
        }
    }
}
