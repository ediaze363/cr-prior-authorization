﻿using Dapper;
using Healthcare.HIS.DataCommon.Persistence;
using Healthcare.HIS.DataOracle.Helpers;
using Healthcare.HIS.DataOracle.Persistence;
using Healthcare.HIS.DataOracle.Resources;
using Healthcare.HIS.Domain.Models;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Healthcare.HIS.DataOracle.Repositories
{
    public class PatientRepository : OracleRepository, IPatientRepository
    {
        public PatientRepository(IConfiguration configuration) : base(configuration)
        {
        }

        public Patient GetById(string documentType, string documentId)
        {
            Patient selected = null;
            documentType = documentType ?? string.Empty;
            documentId = documentId ?? string.Empty;
            using (var database = GetConnection())
            {
                var query = "AUT_P_GET_PATIENT";
                var args = new OracleDynamicParameters();
                args.Add("i_documentType", OracleDbType.Varchar2, ParameterDirection.Input, documentType, documentType.Length);
                args.Add("i_documentId", OracleDbType.Varchar2, ParameterDirection.Input, documentId, documentId.Length);
                args.Add("c_patient", OracleDbType.RefCursor, ParameterDirection.Output);
                selected = SqlMapper.Query<Patient>(database, query, param: args, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return selected;
        }

        public IList<Approval> GetApprovals(string documentType, string documentId)
        {
            IList<Approval> items = null;
            documentType = documentType ?? string.Empty;
            documentId = documentId ?? string.Empty;
            using (var database = GetConnection())
            {
                var query = SqlApproval.GetApprovals;
                var args = new OracleDynamicParameters();
                args.Add("documentType", OracleDbType.Varchar2, ParameterDirection.Input, documentType, documentType.Length);
                args.Add("documentId", OracleDbType.Varchar2, ParameterDirection.Input, documentId, documentId.Length);
                items = SqlMapper.Query<Approval>(database, query, param: args, commandType: CommandType.Text).ToList();
            }
            return items;
        }

        public IList<ApprovalDetail> GetCups(string documentType, string documentId)
        {
            IList<ApprovalDetail> items = null;
            documentType = documentType ?? string.Empty;
            documentId = documentId ?? string.Empty;
            using (var database = GetConnection())
            {
                var query = SqlApproval.GetCups;
                var args = new OracleDynamicParameters();
                args.Add("documentType", OracleDbType.Varchar2, ParameterDirection.Input, documentType, documentType.Length);
                args.Add("documentId", OracleDbType.Varchar2, ParameterDirection.Input, documentId, documentId.Length);
                items = SqlMapper.Query<ApprovalDetail>(database, query, param: args, commandType: CommandType.Text).ToList();
            }
            return items;
        }

        public string Create(Approval approval)
        {
            using (var dataConnect = GetConnection())
            using (var dataDeal = dataConnect.BeginTransaction())
            {
                try
                {
                    approval.ApprovalId = GetSequence(approval.TaxpayerId, dataConnect, dataDeal).ToString();
                    CreateHeader(approval, dataConnect, dataDeal);
                    CreateDetail(approval, dataConnect, dataDeal);
                    dataDeal.Commit();
                }
                catch (System.Exception)
                {
                    dataDeal.Rollback();
                    throw;
                }
            }

            return approval.ApprovalId;
        }

        public IList<Domain.Dtos.ApprovalReportDto> GetApprovalReport()
        {
            IList<Domain.Dtos.ApprovalReportDto> items = null;
            using (var database = GetConnection())
            {
                var query = SqlApproval.GetApprovalReport;
                var args = new OracleDynamicParameters();
                items = SqlMapper.Query<Domain.Dtos.ApprovalReportDto>(database, query, param: null, commandType: CommandType.Text).ToList();
            }
            return items;
        }

        private static int CreateHeader(Approval approval, IDbConnection dataConnect, IDbTransaction dataDeal)
        {
            var sql = SqlApproval.CreateApproval;
            var provider = System.Globalization.CultureInfo.InvariantCulture;
            var dateOfBirth = System.DateTime.ParseExact(approval.Birthdate, "yyyy-MM-ddThh:mm:ss", provider);
            var args = new OracleDynamicParameters();

            args.Add("encautcod", approval.Id);
            args.Add("encautemp", approval.CompanyId);
            args.Add("encautaut", OracleDbType.Long, ParameterDirection.Input, approval.ApprovalId);
            args.Add("encautide", approval.DocumentId);
            args.Add("encauttid", approval.DocumentType);
            args.Add("encautfna", OracleDbType.Date, ParameterDirection.Input, dateOfBirth);
            args.Add("encauthis", OracleDbType.Long, ParameterDirection.Input, approval.History);
            args.Add("encautnum", OracleDbType.Int32, ParameterDirection.Input, approval.Visit);
            args.Add("encautmun", approval.CityId);
            args.Add("encauttip", approval.ApprovalType);
            args.Add("encautmed", approval.PhysicianId);
            args.Add("encautesp", approval.SpecialtyId);
            args.Add("encautdia", approval.DiagnosisId);
            args.Add("encautuad", approval.UserId);
            args.Add("encautips", approval.HeadquarterId);

            var itemsAffected = SqlMapper.Execute(dataConnect, sql, param: args, commandType: CommandType.Text, transaction: dataDeal);
            return itemsAffected;
        }

        private static long GetSequence(string taxpayerId, IDbConnection dataConnect, IDbTransaction dataDeal)
        {
            var sql = "AUT_P_GET_SEQUENCE";
            var args = new OracleDynamicParameters();
            args.Add("i_taxpayerId", OracleDbType.Varchar2, ParameterDirection.Input, taxpayerId);
            args.Add("c_result", OracleDbType.RefCursor, ParameterDirection.Output);
            var selection = SqlMapper.Query<OrgProfile>(dataConnect, sql, 
                param: args, commandType: CommandType.StoredProcedure, transaction: dataDeal).FirstOrDefault();
            return System.Convert.ToInt64(selection.Id);
        }

        private static int CreateDetail(Approval approval, IDbConnection dataConnect, IDbTransaction dataDeal)
        {
            var sql = SqlApproval.CreateDetail;
            int itemsAffected = 0;
            int index = 0;
            foreach (var entry in approval.Details)
            {
                index++;
                var args = new OracleDynamicParameters();
                args.Add("detautaut", approval.Id);
                args.Add("detautlin", OracleDbType.Int32, ParameterDirection.Input, index);
                args.Add("detautcup", entry.Id);
                args.Add("detautcan", OracleDbType.Decimal, ParameterDirection.Input, entry.Quantity);
                args.Add("detautuad", approval.UserId);

                itemsAffected += SqlMapper.Execute(dataConnect, sql, param: args, commandType: CommandType.Text, transaction: dataDeal);
            }
            return itemsAffected;
        }
    }
}
