import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PatientService } from '@app/core/services';
import { ApprovalDataService } from '@app/core/services/approval-data.service';
import { Patient, Profile } from '@app/shared/models';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-approval-search',
  templateUrl: './approval-search.component.html',
  styleUrls: ['./approval-search.component.css']
})
export class ApprovalSearchComponent implements OnInit {
  documentTypes: Profile[] = [];
  searchForm: FormGroup;
  loading = false;
  submitted = false;
  patientSubscription: Subscription;

  get frm() { return this.searchForm.controls; }

  constructor(
    readonly formBuilder: FormBuilder,
    readonly patientService: PatientService,
    readonly toastr: ToastrService,
    readonly data: ApprovalDataService
  ) {
    data.initPatient();
    this.patientSubscription = this.data.getMessage().subscribe(msg => {
      if (msg && (msg.text === 'approvals-loaded')) {
        this.enableSubmit();
      }
    });
  }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      documentType: ['CC', Validators.required],
      documentId: [null, Validators.required]
    });
    this.LoadList();
  }

  LoadList() {
    this.patientService.getDocumentTypes().subscribe(data => {
      this.documentTypes = data;
    });
    this.searchForm.valueChanges.subscribe(() => this.resetForm());
  }

  resetForm() {
    this.data.initPatient();
  }

  onSubmit() {
    if (this.searchForm.invalid || this.submitted) {
      return;
    }

    this.submitted = true;
    this.loading = true;
    const patient = this.getPatientModel();

    this.patientService.getById(patient.documentType, patient.documentId).subscribe(
      data => {
        this.data.patient = data;
        this.data.currentApproval.approvalType = data.admissionType;
        if (!data.hasAllowedAge) {
          this.toastr.info('Paciente menor de edad, no esta autorizado.');
        } else {
          this.toastr.success('Paciente admisionado.');
          this.data.loadApprovals();
        }
      },
      failure => {
        this.toastr.error(failure, 'Buscar paciente');
        this.enableSubmit();
      }
    );
  }

  private getPatientModel() {
    const patient = this.searchForm.value as Patient;
    patient.documentId = patient.documentId.trim();
    this.searchForm.controls['documentId'].setValue(patient.documentId);
    return patient;
  }

  private enableSubmit() {
    this.loading = false;
    this.submitted = false;
  }
}
