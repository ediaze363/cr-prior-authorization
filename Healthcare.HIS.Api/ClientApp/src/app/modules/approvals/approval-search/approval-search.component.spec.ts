import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalSearchComponent } from './approval-search.component';

describe('ApprovalSearchComponent', () => {
  let component: ApprovalSearchComponent;
  let fixture: ComponentFixture<ApprovalSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
