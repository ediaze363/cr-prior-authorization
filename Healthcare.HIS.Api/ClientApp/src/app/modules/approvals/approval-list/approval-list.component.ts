import { Component, OnInit, Input } from '@angular/core';
import { Approval } from '@app/shared/models';
import { ApprovalDataService } from '@app/core/services/approval-data.service';
import { PatientService } from '@app/core/services';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-approval-list',
  templateUrl: './approval-list.component.html',
  styleUrls: ['./approval-list.component.css']
})
export class ApprovalListComponent implements OnInit {
  columnDefs = [
    { headerName: 'Empresa', field: 'companyName' },
    { headerName: 'N.Autorizacion', field: 'approvalId' },
    { headerName: 'Historia.Ing', field: 'historyVisit' },
    { headerName: 'Servicio', field: 'approvalName' },
    { headerName: 'Estado', field: 'statusName' }
  ];

  constructor(
    readonly data: ApprovalDataService,
    readonly patientService: PatientService,
    readonly toastr: ToastrService
  ) {
  }
  loading = false;

  ngOnInit() {
  }

  newApproval() {
    this.loading = true;

    this.patientService.getById(this.data.patient.documentType, this.data.patient.documentId).subscribe(
      result => {
        this.data.newApproval(result);
      },
      err => {
        this.toastr.error(err);
      },
      () => {
        this.loading = false;
      }
    );
  }

  public onApprovalSelected(selectedRow: any) {
    this.data.loadApproval(selectedRow);
  }
}
