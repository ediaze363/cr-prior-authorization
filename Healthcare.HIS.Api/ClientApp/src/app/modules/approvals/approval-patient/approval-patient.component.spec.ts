import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalPatientComponent } from './approval-patient.component';

describe('ApprovalPatientComponent', () => {
  let component: ApprovalPatientComponent;
  let fixture: ComponentFixture<ApprovalPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
