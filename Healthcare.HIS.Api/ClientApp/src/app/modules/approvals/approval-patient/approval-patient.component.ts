import { Component, OnInit } from '@angular/core';
import { ApprovalDataService } from '@app/core/services/approval-data.service';

@Component({
  selector: 'app-approval-patient',
  templateUrl: './approval-patient.component.html',
  styleUrls: ['./approval-patient.component.css']
})
export class ApprovalPatientComponent implements OnInit {

  constructor(
    readonly data: ApprovalDataService
  ) { }

  ngOnInit() {
  }
}
