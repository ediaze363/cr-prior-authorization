import { Component, OnInit, OnDestroy } from '@angular/core';
import { User, Profile, Approval, ApprovalDetail } from '@app/shared/models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PatientService, AuthenticationService } from '@app/core/services';
import { Subscription, Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { ApprovalDataService } from '@app/core/services/approval-data.service';
import { first } from 'rxjs/operators';
import { ConfirmationDialogService } from '@app/shared/components/confirmation-dialog/confirmation-dialog.service';

@Component({
  selector: 'app-approval-detail',
  templateUrl: './approval-detail.component.html',
  styleUrls: ['./approval-detail.component.css']
})
export class ApprovalDetailComponent implements OnInit, OnDestroy {
  form: FormGroup;
  detailForm: FormGroup;
  submitted: boolean;
  userSubscription: Subscription;
  currentUser: User;
  authorizations: Observable<Profile[]>;
  physicians: Observable<Profile[]>;
  specialties: Observable<Profile[]>;
  diagnoses: Observable<Profile[]>;
  companies: Observable<Profile[]>;
  cups: Profile[];
  headquarters: Profile[];
  selectedCup: Profile;
  currentCup: ApprovalDetail;
  columnDefs = [
    { headerName: 'Item', field: 'line' },
    { headerName: 'CUPS', field: 'id' },
    { headerName: 'Descripción', field: 'name' },
    { headerName: 'Cant.', field: 'quantity' },
    { headerName: 'Estado', field: 'statusName' }
  ];
  okCupsLabel = 'Adicionar';
  deleteCupsLabel = 'Retirar';
  patientSubscription: Subscription;
  get frm() { return this.form.controls; }
  get detailFrm() { return this.detailForm.controls; }

  constructor(
    readonly formBuilder: FormBuilder,
    readonly patientService: PatientService,
    readonly authenticationService: AuthenticationService,
    readonly toastr: ToastrService,
    readonly data: ApprovalDataService,
    readonly confirmationDialogService: ConfirmationDialogService
  ) {
    this.userSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
    this.patientSubscription = this.data.getMessage().subscribe(msg => {
      if (msg && (msg.text === 'new-approval' || msg.text === 'edit-approval')) {
        this.initForm();
      }
    });
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      companyId: [null, Validators.required],
      approvalType: [null, Validators.required],
      diagnosisId: [null, Validators.required],
      physicianId: [null, Validators.required],
      specialtyId: [null, Validators.required],
      headquarterId: [null, Validators.required]
    });
    this.detailForm = this.formBuilder.group({
      id: [null, Validators.required],
      line: [null, Validators.required],
      quantity: [null, Validators.required],
      parentId: [null, Validators.nullValidator],
      statusId: [null, Validators.nullValidator],
      statusName: [null, Validators.nullValidator]
    });
    this.initForm();
    this.LoadList();
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.userSubscription.unsubscribe();
    this.patientSubscription.unsubscribe();
  }

  initForm() {
    if (this.data.patient) {
      if (this.data.patient.admissionType) {
        this.form.controls['approvalType'].setValue(this.data.patient.admissionType);
      }
    }
    if (this.data.currentApproval && this.data.patient) {
      this.data.patient.approvalId = this.data.currentApproval.approvalId;
      this.form.controls['companyId'].setValue(this.data.currentApproval.companyId);
      this.form.controls['headquarterId'].setValue(this.data.currentApproval.headquarterId);
      this.form.controls['approvalType'].setValue(this.data.currentApproval.approvalType);
      this.form.controls['diagnosisId'].setValue(this.data.currentApproval.diagnosisId);
      this.form.controls['physicianId'].setValue(this.data.currentApproval.physicianId);
      this.form.controls['specialtyId'].setValue(this.data.currentApproval.specialtyId);
      this.loadListByNit(this.data.currentApproval.taxpayerId);
    }
  }

  LoadList() {
    this.authorizations = this.patientService.getApprovalTypes();
    this.physicians = this.patientService.getPhysicians();
    this.specialties = this.patientService.getSpecialties();
    this.diagnoses = this.patientService.getDiagnoses();
    this.companies = this.patientService.getCompaniesWithApprovals();
  }

  public onChangeCompany($event: any) {
    const company = $event as Profile;
    if (company) {
      const nit = company.id.split(';')[0];
      if (nit !== this.data.currentApproval.taxpayerId) {
        this.data.currentApproval.taxpayerId = nit;
        this.data.initApproval(true);
        this.loadListByNit(nit);
      }
    }
  }

  public onCupSelected($event: any) {
    const detail = $event as ApprovalDetail;
    if (detail.statusId === '1') {
      return;
    }
    this.detailForm.controls['id'].setValue(detail.id);
    this.detailForm.controls['line'].setValue(detail.line);
    this.detailForm.controls['parentId'].setValue(detail.parentId);
    this.detailForm.controls['quantity'].setValue(detail.quantity);
    this.detailForm.controls['statusId'].setValue(detail.statusId);
    this.detailForm.controls['statusName'].setValue(detail.statusName);
    this.selectedCup = detail;
    this.currentCup = detail;
    this.okCupsLabel = 'Modificar';
    this.deleteCupsLabel = detail.parentId ? 'Anular' : 'Retirar';
  }
  private loadListByNit(nit: string) {
    this.patientService.getCups(nit).subscribe(data => {
      this.cups = data;
    });
    this.patientService.getHeadquarters(nit).subscribe(data => {
      this.headquarters = data;
    });
  }

  onChangeCup($event: any) {
    this.selectedCup = $event as Profile;
  }

  onSubmit() {
    this.data.initApproval(true);
    this.submitted = true;

    if (this.form.invalid || this.data.currentApproval.details.length === 0) {
      this.submitted = false;
      this.toastr.info('Todos los campos son requeridos !');
      return;
    }

    const approval = this.parseApprovalModel();
    this.toastr.info('Solicitud en proceso !');
    this.patientService.registerApproval(approval)
      .pipe(first())
      .subscribe(
        () => {
          this.toastr.success('Autorización registrada exitosamente !');
          this.InitializeApproval();
          this.data.loadApprovals();
        },
        failure => {
          this.toastr.error(failure);
        },
        () => {
          this.submitted = false;
        });
  }

  private parseApprovalModel() {
    const approval = this.form.value as Approval;
    approval.details = this.data.currentApproval.details;
    approval.userId = this.currentUser.id;
    approval.documentType = this.data.patient.documentType;
    approval.documentId = this.data.patient.documentId;
    approval.birthdate = this.data.patient.birthdate;
    approval.history = +this.data.patient.history;
    approval.visit = this.data.patient.visit ? +this.data.patient.visit : 0;
    approval.cityId = this.data.patient.cityCode;
    return approval;
  }

  closeApprovalForm() {
    this.data.isNew = false;
    this.data.isEdit = false;
  }

  cleanCupForm() {
    this.selectedCup = new Profile();
    this.currentCup = new ApprovalDetail();
    this.detailForm.reset();
    this.okCupsLabel = 'Adicionar';
  }

  deleteCupForm() {
    const detailItem = this.detailForm.value as ApprovalDetail;
    if (!!detailItem.parentId) {
      this.confirmationDialogService.confirm('Anular CUPS!', '¿Realmente desea anular el registro?', 'Si', 'Cancelar')
      .then((confirmed) => {
        const selected = this.data.currentApproval.details.filter(item => item.line === detailItem.line).pop();
        selected.statusId = '1';
        selected.statusName = 'Anulado';
        this.cleanCupForm();
      })
      .catch(() => console.log(''));
    } else {
      const items = this.data.currentApproval.details.filter(cup => cup.line !== detailItem.line);
      this.data.currentApproval.details = items;
      this.cleanCupForm();
    }
  }

  saveCups() {
    const detailItem = this.detailForm.value as ApprovalDetail;
    const currentCups = (this.selectedCup && !this.selectedCup.name) ? null : this.selectedCup;

    if (detailItem && (!currentCups || !detailItem.id || !detailItem.quantity)) {
      this.toastr.info('Todos los campos son requeridos !');
      return;
    }

    const keyValue = currentCups.name.split('-');
    detailItem.name = keyValue.length > 1 ? keyValue[1].trim() : currentCups.name;
    this.data.initApproval(true);

    if (!detailItem.line) {
      this.AddCupsToApproval(detailItem);
    } else {
      this.updateCupsOnApproval(detailItem);
    }
  }

  private updateCupsOnApproval(detailItem: ApprovalDetail) {
    const item = this.data.currentApproval.details.filter(cup => cup.line === detailItem.line).pop();
    item.id = detailItem.id;
    item.name = detailItem.name;
    item.quantity = detailItem.quantity;
    this.currentCup = item;
    this.cleanCupForm();
  }

  private AddCupsToApproval(detailItem: ApprovalDetail) {
    let newLine = 0;
    for (const item of this.data.currentApproval.details) {
      if (item.line > newLine) {
        newLine = item.line;
      }
    }
    detailItem.line = newLine + 1;
    detailItem.statusId = '0';
    detailItem.statusName = 'Activo';
    this.currentCup = detailItem;
    this.cleanCupForm();
    this.data.currentApproval.details.push(detailItem);
  }

  InitializeApproval() {
    this.data.initApproval();
    this.currentCup = new ApprovalDetail();
    this.cleanCupForm();
    this.form.reset();
    this.closeApprovalForm();
  }
}
