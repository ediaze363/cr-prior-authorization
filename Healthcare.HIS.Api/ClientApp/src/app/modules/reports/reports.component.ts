import { Component, OnInit } from '@angular/core';
import { PatientService } from '@app/core/services';
import { Observable } from 'rxjs';
import { ApprovalReport } from '../../shared/models/approval-report';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  items: Observable<ApprovalReport[]>;
  columnDefs = [
    { headerName: 'C_EMPRESA', field: 'C_EMPRESA' },
    { headerName: 'N_EMPRESA', field: 'N_EMPRESA' },
    { headerName: 'NIT_EMPRESA', field: 'NIT_EMPRESA' },
    { headerName: 'N_AUTORIZACION', field: 'N_AUTORIZACION' },
    { headerName: 'TIPO_IDE', field: 'TIPO_IDE' },
    { headerName: 'IDE', field: 'IDE' },
    { headerName: 'N_PACIENTE', field: 'N_PACIENTE' },
    { headerName: 'HISTORIA', field: 'HISTORIA' },
    { headerName: 'INGRESO', field: 'INGRESO' },
    { headerName: 'TIPO_AUT', field: 'TIPO_AUT' },
    { headerName: 'N_TIPO', field: 'N_TIPO' },
    { headerName: 'C_MEDICO', field: 'C_MEDICO' },
    { headerName: 'N_MEDICO', field: 'N_MEDICO' },
    { headerName: 'C_ESPECIALIDAD', field: 'C_ESPECIALIDAD' },
    { headerName: 'N_ESPECIALIDAD', field: 'N_ESPECIALIDAD' },
    { headerName: 'C_DIAGNOSTICO', field: 'C_DIAGNOSTICO' },
    { headerName: 'N_DIAGNOSTICO', field: 'N_DIAGNOSTICO' },
    { headerName: 'ITEM', field: 'ITEM' },
    { headerName: 'C_CUP', field: 'C_CUP' },
    { headerName: 'N_CUP', field: 'N_CUP' },
    { headerName: 'TP_CUP', field: 'TP_CUP' },
    { headerName: 'GRU_CUP', field: 'GRU_CUP' }
  ];

  constructor(readonly patientService: PatientService) { }

  ngOnInit() {
    // this.items =
  }

  runReport() {
    const rows = this.patientService.getApprovalReport();
    console.log(rows);
  }
}
