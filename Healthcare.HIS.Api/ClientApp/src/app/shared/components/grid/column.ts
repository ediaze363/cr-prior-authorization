/* sample https://medium.com/ag-grid/get-started-with-angular-grid-in-5-minutes-83bbb14fac93 */
export class Column {
  public field: string;
  public headerName: string;
}
