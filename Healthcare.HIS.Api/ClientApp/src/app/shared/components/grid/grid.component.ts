import { Component } from '@angular/core';
import { Column } from './column';
import { Sorter } from './sorter';
import { EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html'
})
export class GridComponent {
  public sorter: Sorter = new Sorter();
  public allRows: Array<any>;
  public pages = 5;
  public defaultPages = 10;
  public pageSize = 5;
  public rowsCount = 0;
  public currentPage = 1;
  public pagesIndex: Array<number>;
  public pageStart = 1;
  public pageSizes: number[] = [5, 10, 20, 50];

  @Input() columnDefs: Array<Column>;
  @Input() rowData: Array<any>;
  @Input() itemsCount: number;
  @Input() enablePageSizeChange: boolean;
  @Input() set paging(data: Array<any>) {
    this.allRows = data;
    if (data) {
      this.setPagination();
    }
  }
  @Output() selectedEvent = new EventEmitter<any>();

  constructor() {
  }
  public sort(key: string) {
    this.sorter.sort(key, this.rowData);
  }
  public select(row: any) {
    this.selectedEvent.emit(row);
  }
  private setPagination() {
    this.currentPage = 1;
    this.pageStart = 1;
    this.rowsCount = Math.ceil(this.allRows.length / this.pageSize);

    if (this.pages < this.defaultPages) {
      this.pages = this.defaultPages;
    }
    if (this.rowsCount < this.pages) {
      this.pages = this.rowsCount;
    }
    if (this.pages === 0) {
      this.currentPage = 0;
    }
    this.setPageRows();
  }
  public setPageRows() {
    this.rowData = this.allRows.slice((this.currentPage - 1) * this.pageSize, (this.currentPage) * this.pageSize);
    this.pagesIndex = this.getPageItems();
    this.itemsCount = this.allRows.length;
  }
  public getPageItems(): any {
    const pageItems = new Array();
    for (let index = this.pageStart; index < this.pageStart + this.pages; index++) {
      pageItems.push(index);
    }
    return pageItems;
  }
  public prevPage() {
    if (this.currentPage > 1) {
      this.currentPage--;
    }
    if (this.currentPage < this.pageStart) {
      this.pageStart = this.currentPage;
    }
    this.setPageRows();
  }
  public nextPage() {
    if (this.currentPage < this.rowsCount) {
      this.currentPage++;
    }
    if (this.currentPage >= (this.pageStart + this.pages)) {
      this.pageStart = this.currentPage - this.pages + 1;
    }
    this.setPageRows();
  }
  public setPage(index: number) {
    this.currentPage = index;
    this.setPageRows();
  }
  public setPageSize(value: number) {
    this.pageSize = value;
    if (this.allRows) {
      this.setPagination();
      this.setPage(1);
    }
  }
}
