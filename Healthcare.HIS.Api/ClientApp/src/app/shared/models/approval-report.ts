export class ApprovalReport {
  C_EMPRESA: string;
  N_EMPRESA: string;
  NIT_EMPRESA: string;
  N_AUTORIZACION: string;
  TIPO_IDE: string;
  IDE: string;
  N_PACIENTE: string;
  HISTORIA: string;
  INGRESO: string;
  TIPO_AUT: string;
  N_TIPO: string;
  C_MEDICO: string;
  N_MEDICO: string;
  C_ESPECIALIDAD: string;
  N_ESPECIALIDAD: string;
  C_DIAGNOSTICO: string;
  N_DIAGNOSTICO: string;
  ITEM: string;
  C_CUP: string;
  N_CUP: string;
  TP_CUP: string;
  GRU_CUP: string;
}
