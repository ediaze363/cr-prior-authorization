import { ApprovalDetail } from '.';

export class Approval {
  age: number;
  approvalId: number;
  approvalName: string;
  approvalType: string;
  birthdate: string;
  cityId: string;
  companyId: string;
  companyName: string;
  created: string;
  diagnosisId: string;
  documentId: string;
  documentType: string;
  history: number;
  id: string;
  physicianId: string;
  specialtyId: string;
  stateId: string;
  statusId: string;
  statusName: string;
  visit: number;
  historyVisit: string;
  active: string;
  userId: string;
  headquarterId: string;
  taxpayerId: string;
  details: ApprovalDetail[];
}
