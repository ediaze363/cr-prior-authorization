export class Patient {
  id: string;
  nationalId: string;
  today: string;
  now: string;
  dateOfBirth: string;
  documentType: string;
  documentId: string;
  history: string;
  visit: string;
  name: string;
  age: string;
  address: string;
  phoneNumber: string;
  stateCode: string;
  stateName: string;
  cityCode: string;
  cityName: string;
  hasAllowedAge: boolean;
  admissionType: string;
  approvalId: number;
  isNew: boolean;
  birthdate: string;
}
