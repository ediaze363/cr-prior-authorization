export class ApprovalDetail {
  id: string;
  line: number;
  name: string;
  quantity: number;
  statusId: string;
  statusName: string;
  parentId: string;
}
