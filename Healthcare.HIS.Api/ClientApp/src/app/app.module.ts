import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { NgSelectModule } from '@ng-select/ng-select';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './core/nav-menu/nav-menu.component';
import { HomeComponent } from './modules/home/home.component';
import { routing } from './app.routing';
import { ErrorInterceptor, JwtInterceptor } from './core/interceptors';
import { LoginComponent } from './modules/login';
import { FooterBarComponent } from './core/footer-bar/footer-bar.component';
import { ApprovalsComponent } from './modules/approvals/approvals.component';
import { ApprovalListComponent } from './modules/approvals/approval-list/approval-list.component';
import { PatientService } from './core/services';
import { AlertComponent, GridComponent } from './shared/components';
import { ApprovalDetailComponent } from './modules/approvals/approval-detail/approval-detail.component';
import { ReportsComponent } from './modules/reports/reports.component';
import { ApprovalSearchComponent } from './modules/approvals/approval-search/approval-search.component';
import { ApprovalPatientComponent } from './modules/approvals/approval-patient/approval-patient.component';
import { ConfirmationDialogComponent } from './shared/components/confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from './shared/components/confirmation-dialog/confirmation-dialog.service';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    GridComponent,
    NavMenuComponent,
    HomeComponent,
    LoginComponent,
    FooterBarComponent,
    ApprovalsComponent,
    ApprovalListComponent,
    ApprovalDetailComponent,
    ReportsComponent,
    ApprovalSearchComponent,
    ApprovalPatientComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgbModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    PatientService,
    ConfirmationDialogService
  ],
  bootstrap: [AppComponent],
  entryComponents: [ ConfirmationDialogComponent ]
})
export class AppModule { }
