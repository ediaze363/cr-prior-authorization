import { Component } from '@angular/core';
import { User } from '@app/shared/models';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/core/services';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;
  currentUser: User;

  constructor(
    readonly router: Router,
    readonly authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(usr => {
      this.currentUser = usr;
    });
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
