import { Component, OnInit } from '@angular/core';
import { User } from '@app/shared/models';
import { AuthenticationService } from '../services';

@Component({
  selector: 'app-footer-bar',
  templateUrl: './footer-bar.component.html',
  styleUrls: ['./footer-bar.component.css']
})
export class FooterBarComponent implements OnInit {
  currentUser: User;

  constructor(readonly authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(usr => {
      this.currentUser = usr;
    });
  }

  ngOnInit() {
  }

}
