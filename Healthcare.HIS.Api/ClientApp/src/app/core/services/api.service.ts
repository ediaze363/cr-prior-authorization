import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(
    readonly http: HttpClient,
    @Inject('BASE_URL') readonly baseUrl: string) {
  }

  protected handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('A client-side or network error occurred. Handle it accordingly', error.error.message);
    } else {
      console.error(`The backend returned an unsuccessful response code. ${error.status}, ` + `body: ${error.error}`);
    }
    return throwError(error);
  }
}
