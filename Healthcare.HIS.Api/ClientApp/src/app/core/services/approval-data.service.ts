import { Injectable } from '@angular/core';
import { Patient, Approval } from '@app/shared/models';
import { PatientService } from '.';
import { ToastrService } from 'ngx-toastr';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApprovalDataService {
  public patient: Patient;
  public approvals: Approval[];
  public currentApproval: Approval;
  public isNew: boolean;
  public isEdit: boolean;
  private subject = new Subject<any>();

  constructor(
    readonly patientService: PatientService,
    readonly toastr: ToastrService) { }

  public loadApprovals() {
    this.patientService.getApprovals(this.patient.documentType, this.patient.documentId)
    .subscribe(
      items => {
        this.approvals = items;
        this.sendMessage('approvals-loaded');
      },
      failure => { this.toastr.error(failure); }
    );
  }

  public sendMessage(message: string) {
    this.subject.next({ text: message });
  }

  public clearMessages() {
    this.subject.next();
  }

  public getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  public newApproval(patientDto: Patient) {
    this.isNew = true;
    this.isEdit = false;
    this.patient = patientDto;
    this.patient.approvalId = 0;
    this.initApproval();
    this.currentApproval.approvalType = patientDto.admissionType;
    this.currentApproval.history = +patientDto.history;
    this.currentApproval.visit = +patientDto.visit;
    this.sendMessage('new-approval');
  }

  public initPatient() {
    this.patient = null;
    this.approvals = [];
    this.isNew = false;
    this.isEdit = false;
    this.initApproval();
  }

  public initApproval(preserveInstance: boolean = false) {
    if (preserveInstance) {
      if (!this.currentApproval) {
        this.currentApproval = new Approval();
      }
      if (!this.currentApproval.details) {
        this.currentApproval.details = [];
      }
    } else {
      this.currentApproval = new Approval();
      this.currentApproval.details = [];
    }
  }

  public loadApproval(approval: Approval) {
    this.currentApproval = approval;
    this.isNew = false;
    this.isEdit = true;
    this.sendMessage('edit-approval');
  }
}
