import { Injectable, Component, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Profile, Patient, Approval, ApprovalReport } from '@app/shared/models';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  constructor(
    readonly http: HttpClient,
    readonly toastr: ToastrService,
    @Inject('BASE_URL') readonly baseUrl: string) {
  }

  public getDocumentTypes() {
    return this.http.get<Profile[]>(`${this.baseUrl}api/Patients/documentTypes`)
      .pipe(catchError(this.handleError));
  }

  public getApprovalTypes(): Observable<Profile[]> {
    return this.http.get<Profile[]>(`${this.baseUrl}api/Patients/approvalTypes`)
      .pipe(catchError(this.handleError));
  }

  public getPhysicians(): Observable<Profile[]> {
    return this.http.get<Profile[]>(`${this.baseUrl}api/Patients/physicians`)
      .pipe(catchError(this.handleError));
  }

  public getDiagnoses(): Observable<Profile[]> {
    return this.http.get<Profile[]>(`${this.baseUrl}api/Patients/diagnoses`)
      .pipe(catchError(this.handleError));
  }

  public getSpecialties(): Observable<Profile[]> {
    return this.http.get<Profile[]>(`${this.baseUrl}api/Patients/specialties`)
      .pipe(catchError(this.handleError));
  }

  public getCompaniesWithApprovals(): Observable<Profile[]> {
    return this.http.get<Profile[]>(`${this.baseUrl}api/Patients/CompanyWithApprovals`)
      .pipe(catchError(this.handleError));
  }

  public getCups(companyNit: string) {
    return this.http.get<Profile[]>(`${this.baseUrl}api/Patients/Cups/${companyNit}`)
      .pipe(catchError(this.handleError));
  }

  public getHeadquarters(companyNit: string) {
    return this.http.get<Profile[]>(`${this.baseUrl}api/Patients/Ips/${companyNit}`)
      .pipe(catchError(this.handleError));
  }

  public getById(type: string, id: string) {
    return this.http.get<Patient>(`${this.baseUrl}api/Patients/${type}/${id}`)
      .pipe(catchError(this.handleError));
  }

  public getApprovals(type: string, id: string) {
    return this.http.get<Approval[]>(`${this.baseUrl}api/Patients/Approvals/${type}/${id}`)
      .pipe(catchError(this.handleError));
  }

  public register(model: Profile) {
    return this.http.post(`${this.baseUrl}api/users/register`, model)
      .pipe(catchError(this.handleError));
  }

  public update(model: Profile) {
    return this.http.put(`${this.baseUrl}api/users/${model.id}`, model)
      .pipe(catchError(this.handleError));
  }

  public delete(id: number) {
    return this.http.delete(`${this.baseUrl}api/users/${id}`)
      .pipe(catchError(this.handleError));
  }

  public registerApproval(model: Approval) {
    return this.http.post(`${this.baseUrl}api/Patients/Approvals`, model)
      .pipe(catchError(this.handleError));
  }

  public getApprovalReport() {
    return this.http.get<ApprovalReport[]>(`${this.baseUrl}api/Patients/ApprovalReport`)
      .pipe(catchError(this.handleError));
  }

  private handleError(failure: HttpErrorResponse) {
    if (failure.error instanceof ErrorEvent) {
      console.error('A client-side or network error occurred. Handle it accordingly', failure.error.message);
    } else {
      if (failure.status) {
        console.error(`The backend returned an unsuccessful response code ${failure.status}, ` + `body: ${failure.error}`);
      }
    }
    return throwError(failure);
  }
}
