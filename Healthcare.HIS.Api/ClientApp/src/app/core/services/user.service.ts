import { Injectable, Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '@app/shared/models';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string) {
  }

  getAll() {
    return this.http.get<User[]>(`${this.baseUrl}/api/users`);
  }

  getById(id: number) {
      return this.http.get(`${this.baseUrl}/api/users/${id}`);
  }

  register(user: User) {
      return this.http.post(`${this.baseUrl}/api/users/register`, user);
  }

  update(user: User) {
      return this.http.put(`${this.baseUrl}/api/users/${user.id}`, user);
  }

  delete(id: number) {
      return this.http.delete(`${this.baseUrl}/api/users/${id}`);
  }
}
