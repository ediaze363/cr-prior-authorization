import { TestBed } from '@angular/core/testing';

import { ApprovalDataService } from './approval-data.service';

describe('ApprovalDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApprovalDataService = TestBed.get(ApprovalDataService);
    expect(service).toBeTruthy();
  });
});
