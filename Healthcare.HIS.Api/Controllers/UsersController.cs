﻿using AutoMapper;
using Healthcare.HIS.Api.Helpers;
using Healthcare.HIS.Business.Interfaces;
using Healthcare.HIS.Domain.Dtos;
using Healthcare.HIS.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Healthcare.HIS.Api.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : BaseApiController<IUserService, UserDto>
    {
        private readonly AppSettings _appSettings;

        public UsersController(
            IUserService userService, 
            IMapper mapper, 
            IOptions<AppSettings> appSettings) : base(userService, mapper)
        {
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Valida que el usuario se encuentre registrado en la base de datos de Servinte
        /// </summary>
        /// <param name="userDto">DTO con los datos del usuario Servinte</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]UserDto userDto)
        {
            const string invalidSalt = "El código del cliente es requerido en el archivo de configuración de la aplicación.";
            const string invalidUser = "El usuario o la clave son incorrectos.";

            if (string.IsNullOrEmpty(_appSettings.PasswordSalt))
            {
                return BadRequest(new { message = invalidSalt });
            }

            var user = _businessService.Authenticate(userDto.Username, userDto.Password, _appSettings.PasswordSalt);

            if (user == null)
            {
                return BadRequest(new { message = invalidUser });
            }

            string tokenString = CreateToken(user);

            // return basic user info (without password) and token to store client side
            return Ok(new
            {
                user.Id,
                user.Username,
                user.FirstName,
                user.LastName,
                Token = tokenString
            });
        }

        [HttpGet("{id}")]
        public IActionResult GetUserById(string id)
        {
            var user = _businessService.GetById(id);
            var userDto = _mapper.Map<UserDto>(user);
            return Ok(userDto);
        }

        private string CreateToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.PasswordSecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature
                )
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }
    }
}
