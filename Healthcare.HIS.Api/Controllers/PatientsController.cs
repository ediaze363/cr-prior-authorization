﻿using AutoMapper;
using Healthcare.HIS.Api.Helpers;
using Healthcare.HIS.Business.Interfaces;
using Healthcare.HIS.Domain.Dtos;
using Healthcare.HIS.Domain.Enums;
using Healthcare.HIS.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Healthcare.HIS.Api.Controllers
{
    // https://www.freecodecamp.org/news/an-awesome-guide-on-how-to-build-restful-apis-with-asp-net-core-87b818123e28/
    [Route("api/[controller]")]
    public class PatientsController : BaseApiController<IPatientService, PatientDto>
    {
        private readonly IOrgProfileService _orgProfileService;

        public PatientsController(
            IPatientService patientService,
            IMapper mapper,
            IOrgProfileService orgProfileService) : base(patientService, mapper)
        {
            _orgProfileService = orgProfileService;
        }

        // GET: api/Patients/CC/123
        [HttpGet("{documentType}/{documentId}")]
        [AllowAnonymous]
        public IActionResult Get(string documentType, string documentId)
        {
            var entity = _businessService.GetById(documentType, documentId);
            if (entity == null || (entity != null && string.IsNullOrEmpty(entity.AdmissionType)))
            {
                return NotFound(new ApiResponse(404, "Paciente no admisionado."));
            }

            var entityDto = _mapper.Map<PatientDto>(entity);
            return Ok(entityDto);
        }

        // GET: api/Patients/Order/CC/123
        [HttpGet("Approvals/{documentType}/{documentId}")]
        [AllowAnonymous]
        public IActionResult GetApprovals(string documentType, string documentId)
        {
            var items = _businessService.GetApprovals(documentType, documentId);
            var result = _mapper.Map<IList<ApprovalDto >>(items);
            return Ok(result);
        }

        // GET: api/documentTypes
        [AllowAnonymous]
        [HttpGet("documentTypes")]
        public IActionResult GetDocumentTypes()
        {
            const string documentTypes = "TIPIDE";
            var items = _orgProfileService.GetById(documentTypes);
            if (items == null)
            {
                return NotFound(new ApiResponse(404, "Tipos de documento no encontrados."));
            }
            var itemsDto = _mapper.Map<IEnumerable<OrgProfileDto>>(items);
            return Ok(itemsDto);
        }

        [AllowAnonymous]
        [HttpGet("approvalTypes")]
        public IActionResult GetAuthorizationTypes()
        {
            return GetMasterItemTypes(MasterItemType.AutTip, "Autorizaciones");
        }

        [AllowAnonymous]
        [HttpGet("diagnoses")]
        public IActionResult GetDiagnosisTypes()
        {
            return GetMasterItemTypes(MasterItemType.InDia, "Diagnosticos");
        }

        [AllowAnonymous]
        [HttpGet("specialties")]
        public IActionResult GetSpecialityTypes()
        {
            return GetMasterItemTypes(MasterItemType.InEsp, "Especialidades");
        }

        [AllowAnonymous]
        [HttpGet("companies")]
        public IActionResult GetCompanyTypes()
        {
            return GetMasterItemTypes(MasterItemType.InEmp, "Empresas");
        }

        [AllowAnonymous]
        [HttpGet("CompanyWithApprovals")]
        public IActionResult GetCompanyWithApprovals()
        {
            return GetMasterItemTypes(MasterItemType.AutEmp, "Empresas por Autorización");
        }

        [AllowAnonymous]
        [HttpGet("physicians")]
        public IActionResult GetPhysicianTypes()
        {
            return GetMasterItemTypes(MasterItemType.InMed, "Médicos");
        }

        [HttpGet("Cups/{companyNit}")]
        [AllowAnonymous]
        public IActionResult GetCups(string companyNit)
        {
            return GetMasterItemTypes(MasterItemType.AutCup, "CUPS por Nit", companyNit);
        }

        [HttpGet("Ips/{companyNit}")]
        [AllowAnonymous]
        public IActionResult GetIps(string companyNit)
        {
            return GetMasterItemTypes(MasterItemType.AutIps, "IPS por Nit", companyNit);
        }

        [HttpPost("Approvals")]
        [AllowAnonymous]
        public IActionResult Create([FromBody] ApprovalDto approvalDto)
        {
            if (approvalDto == null)
            {
                return BadRequest(new ApiResponse(400, "DTO autorización nulo."));
            }

            var approval = _mapper.Map<Approval>(approvalDto);
            var companyCodes = approvalDto.CompanyId.Split(';');
            approval.TaxpayerId = companyCodes[0];
            approval.CompanyId = companyCodes[1];

            approval = _businessService.Create(approval);
            approvalDto = _mapper.Map<ApprovalDto>(approval);

            return Ok(approvalDto);
        }

        [HttpGet("ApprovalReport")]
        [AllowAnonymous]
        public IActionResult GetApprovalReport()
        {
            var items = _businessService.GetApprovalReport();
            return Ok(items);
        }

        private IActionResult GetMasterItemTypes(MasterItemType typeId, string entityName, string companyNit = null)
        {
            var items = _orgProfileService.GetByType(typeId, companyNit);
            if (items == null)
            {
                var message = !string.IsNullOrEmpty(companyNit) ? $" para el NIT {companyNit}" : string.Empty;
                message = $"No existen registros para el maestro {entityName}{message}.";
                return NotFound(new ApiResponse(404, message));
            }
            var itemsDto = _mapper.Map<IEnumerable<OrgProfileDto>>(items);
            return Ok(itemsDto);
        }
    }
}
