﻿using AutoMapper;
using Healthcare.HIS.Domain.Dtos;
using Healthcare.HIS.Domain.Models;

namespace Healthcare.HIS.Api.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
            CreateMap<Patient, PatientDto>().
                ForMember(d => d.Id, m => m.MapFrom(s => s.History));
            CreateMap<PatientDto, Patient>().
                ForMember(d => d.History, m => m.MapFrom(s => s.Id));
            CreateMap<OrgProfile, OrgProfileDto>();
            CreateMap<OrgProfileDto, OrgProfile>();
            CreateMap<Approval, ApprovalDto>();
            CreateMap<ApprovalDto , Approval>();
            CreateMap<ApprovalDetail, ApprovalDetailDto>();
            CreateMap<ApprovalDetailDto, ApprovalDetail>();
        }
    }
}
