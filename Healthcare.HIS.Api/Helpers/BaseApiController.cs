﻿using AutoMapper;
using Healthcare.HIS.Api.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Healthcare.HIS.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [ValidationExceptionFilter]
    public class BaseApiController<TService, TDto> : ControllerBase
    {
        protected readonly TService _businessService;
        protected readonly IMapper _mapper;

        public BaseApiController(TService businessService, IMapper mapper)
        {
            _businessService = businessService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TDto>> Get()
        {
            throw new NotImplementedException();
        }
    }
}