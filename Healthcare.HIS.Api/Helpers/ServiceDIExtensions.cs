﻿using Healthcare.HIS.Business.Interfaces;
using Healthcare.HIS.Business.Repositories;
using Healthcare.HIS.Business.Services;
using Healthcare.HIS.DataCommon.Context;
using Healthcare.HIS.DataCommon.Interfaces;
using Healthcare.HIS.DataCommon.Persistence;
using Healthcare.HIS.DataOracle.Context;
using Healthcare.HIS.DataOracle.Repositories;
using Healthcare.Legacy.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Healthcare.HIS.Api.Helpers
{
    public static class ServiceDIExtensions
    {
        public static void ConfigureDiForApplicationServices(this IServiceCollection services,
            AppSettings appSettings, IConfiguration configuration)
        {
            services.AddSingleton(configuration);
            services.AddScoped<ICryptoHelper, CryptoHelperOraMsSql>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPatientService, PatientService>();
            services.AddScoped<IOrgProfileService, OrgProfileService>();            

            switch (appSettings.DefaultConnection.ToUpper())
            {
                case "ORACLE":
                    services.AddScoped<IDatabaseContext, OracleDbContext>();
                    services.AddScoped<IUserRepository, UserRepository>();
                    services.AddScoped<IPatientRepository, PatientRepository>();
                    services.AddScoped<IOrgProfileRepository, OrgProfileRepository>();
                    break;
                case "SQLSERVER":
                case "LOCALDB":
                    services.AddScoped<IDatabaseContext, RepositoryDbContext>();
                    services.AddScoped<IUserRepository, UserSqlRepository>();
                    break;
                default:
                    break;
            }
        }
    }
}
