﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Healthcare.HIS.Api.Helpers
{
    public class ValidationExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            SetResult(context);
        }

        private static void SetResult(ExceptionContext context)
        {
            var exception = context.Exception.GetBaseException();            
            if (exception != null)
            {
                context.Result = new BadRequestObjectResult(new ApiResponse(500, exception.Message));
            }
        }
    }
}
