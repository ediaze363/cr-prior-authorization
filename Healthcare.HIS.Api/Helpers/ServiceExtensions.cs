﻿using Healthcare.HIS.Business.Interfaces;
using Healthcare.HIS.DataCommon.Context;
using Healthcare.HIS.DataOracle.Context;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;

namespace Healthcare.HIS.Api.Helpers
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
        }

        public static void GlobalCorsPolicy(this IApplicationBuilder app)
        {
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
        }        

        public static void AddDatabasePersistency(this IServiceCollection services, 
            AppSettings appSettings, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString(appSettings.DefaultConnection);
            switch (appSettings.DefaultConnection.ToUpper())
            {
                case "ORACLE":
                    services.AddDbContext<OracleDbContext>(options => options.UseOracle(connectionString));
                    break;
                case "SQLSERVER":
                case "LOCALDB":
                    services.AddDbContext<RepositoryDbContext>(options => options.UseSqlServer(connectionString));
                    break;
                default:
                    services.AddDbContext<RepositoryDbContext>(x => x.UseInMemoryDatabase("DB"));
                    break;
            }
        }

        public static void ConfigureJwtAuthentication(this IServiceCollection services, AppSettings appSettings)
        {
            var secretKey = System.Text.Encoding.ASCII.GetBytes(appSettings.PasswordSecretKey);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context => { return ValideAuthorizationToken(context); }
                };
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(secretKey),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        public static void ConfigureSwaggerService(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Prior Authorization API",
                    Description = "Prior Authorization Process",
                    TermsOfService = "None"
                });
            });
        }

        private static System.Threading.Tasks.Task ValideAuthorizationToken(TokenValidatedContext context)
        {
            var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();
            var userId = context.Principal.Identity.Name;
            var user = userService.GetById(userId);
            if (user == null)
            {
                // return unauthorized if user no longer exists
                context.Fail("Unauthorized");
            }
            return System.Threading.Tasks.Task.CompletedTask;
        }
    }
}
