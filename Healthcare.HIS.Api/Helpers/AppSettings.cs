namespace Healthcare.HIS.Api.Helpers
{
    /// <summary>
    /// Configuration mapper for appsettings.json file
    /// </summary>
    public class AppSettings
    {
        /// <value>
        /// Private key for encryption.
        /// see tutorial http://jasonwatmore.com/post/2018/10/29/angular-7-user-registration-and-login-example-tutorial
        /// </value>
        public string PasswordSecretKey { get; set; }

        /// <value>
        /// Private salt for encryption.
        /// </value>
        public string PasswordSalt { get; set; }

        /// <value>
        /// Default conection string
        /// See providers list https://docs.microsoft.com/en-us/ef/core/providers/index
        /// </value>
        public string DefaultConnection { get; set; }

        /// <value>
        /// Default schema to access database
        /// </value>
        public string DefaultSchema { get; set; }
    }
}