using Healthcare.Legacy.Security;
using Xunit;

namespace Healthcare.HIS.Test
{
    public class SecurityTest
    {
        private const string _salt = "CREMEC";

        [Fact]
        public void EncodeOraclePasswordTest()
        {
            const string plainText = "abuela15";
            const string expected = ")9?76)t";
            ICryptoHelper crypto = new CryptoHelperOraMsSql();

            var actual = crypto.Encrypt(plainText, _salt);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ReversePasswordTest()
        {
            const string expected = "abuela15";
            const string encryptedText = ")9?76)t"; // jhcortez
            ICryptoHelper crypto = new CryptoHelperOraMsSql();

            var actual = crypto.Decrypt(encryptedText, _salt);

            Assert.Equal(expected, actual);
        }
    }
}
